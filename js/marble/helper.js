/**
 * Helper
 */
function assign(value, defaultValue) {

  if (typeof value === 'undefined') {
    return defaultValue;
  } else {
    return value;
  }
}