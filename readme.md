## The Marbleous Beat Machine

Having fun with HTML5 Canvas ended up in a in little app where marbles fall down and you can create crazy rhytms by adding and moving bars on the screen:

[The Marbleous Beat Machine](https://marble.hendrikgoebel.de)

### How to use it
Draw some blocks on the canvas and see what happens. 

Doubleclick removes a block.

Its more exciting when you turn on audio.


### How to install 
just download and run it using your favorite server.

e.g.:

```
php -S localhost:9001
```


Then open in your browser localhost:9001

### Samples
Samples are not included in the repo. The ones used on the website I downloaded from [Music Radar](https://www.musicradar.com/news/tech/free-music-samples-royalty-free-loops-hits-and-multis-to-download)

Put them into the samples folder
then run "php createsoundtable.php" to generate the soundtable. 


